# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import Bundle,collection,owner
# Register your models here.
class bundleadmin(admin.ModelAdmin):
    pass
    list_display = ('name','updated','created','asset','version','collection')
    list_filter = ('name','updated','created','version')
    search_fields = ('name','updated','version','collection')
# Register your models here.
class collectionadmin(admin.ModelAdmin):
    pass
    list_display = ('nomcollection','updatedcollection','createdcollection')
    list_filter = ('nomcollection','updatedcollection','createdcollection')
    search_fields = ('nomcollection','updatedcollection','createdcollection')

class owneradmin(admin.ModelAdmin):
    pass
    list_display = ('nom','updatedadmin','createdadmin')
    list_filter = ('nom','updatedadmin','createdadmin')
    search_fields = ('nom','updatedadmin','createdadmin')
# Register your models here.
admin.site.register(collection,collectionadmin)
admin.site.register(Bundle,bundleadmin)
admin.site.register(owner,owneradmin)

