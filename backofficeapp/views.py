
from django.http import HttpResponse
from django.views.generic.base import TemplateView


def index(request):
    return HttpResponse("<h1>khra</h1>")


class HomeView(TemplateView):

    template_name = 'backofficeapp/index.html'
