# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-03-12 07:34
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backofficeapp', '0003_auto_20180312_0830'),
    ]

    operations = [
        migrations.RenameField(
            model_name='owner',
            old_name='created',
            new_name='createdadmin',
        ),
        migrations.RenameField(
            model_name='owner',
            old_name='updated',
            new_name='updatedadmin',
        ),
    ]
