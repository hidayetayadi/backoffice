# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

#propritaire de contenu
class owner(models.Model):
    nom =models.CharField(max_length=100)
    videoouimagedepromotion=models.FileField(upload_to='publiciter/propritaire')
    createdadmin = models.DateTimeField(auto_now_add=True)
    updatedadmin = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.nom

# collection
class collection(models.Model):
    nomcollection = models.CharField(max_length=100)
    createdcollection = models.DateTimeField(auto_now_add=True)
    updatedcollection = models.DateTimeField(auto_now=True)
    adress = models.CharField(max_length=100)
    videoouimagedepromotion=models.FileField(upload_to='publiciter/collection')
    propritaire = models.ForeignKey(owner)
    def __str__(self):
        return self.nomcollection

#contenu
class Bundle (models.Model):
    name=models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now= True)
    asset = models.FileField(upload_to='assetes')
    version = models.IntegerField()
    size=models.FloatField()
    collection = models.ForeignKey(collection)
    videoouimagedepromotion=models.FileField(upload_to='publiciter/contenu')
    def __str__(self):
        return self.name


class type(models.Model):
    nomtype = models.IntegerField()


